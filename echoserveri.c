#include "csapp.h"
#include <string.h>

void echo(int connfd);

void sigchld_handler(int sig);

void strip(char *s);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];
	Signal(SIGCHLD, sigchld_handler);
	listenfd = Open_listenfd(port);
	
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);
		if (Fork()==0){
			Close(listenfd);
			echo(connfd);
			Close(connfd);
			exit(0);
		}
		Close(connfd);
	}
	exit(0); 
}

void sigchld_handler(int sig){
	while(waitpid(-1,0,WNOHANG)>0);
	return;
}

void echo(int connfd)
{
	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	char* resp;
	

	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		strip(buf);
		char* const data[] = {buf, NULL};
		
		if (Fork()==0){
			Execve(buf, data, NULL);
			/*enviar senal*/
			//response="ERROR";
			//response="OK";
			exit(0);
		}
		
		Rio_writen(connfd, buf, strlen(buf));
		printf("server received %lu bytes\n", n);

	}
}


void strip(char *s) {
    char *p2 = s;
    while(*s != '\0') {
        if(*s != '\n') {
            *p2++ = *s++;
        } else {
            ++s;
        }
    }
    *p2 = '\0';
}
